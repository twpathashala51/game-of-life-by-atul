package twpathashala51.GameOfLife;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GameOfLifeTest {

	@Test
	public void toCheckOnlyForBlockPattern(){
		GameOfLife gamer=new GameOfLife();
		gamer.createCells(1,1);
		gamer.createCells(1,2);
		gamer.createCells(2,1);
		gamer.createCells(2,2);
		gamer.campare(1,1);
		gamer.campare(1,2);
		gamer.campare(2,1);
		gamer.campare(2,2);
			GameOfLife anotherGamer=new GameOfLife();
		anotherGamer.createCells(1,1);
		anotherGamer.createCells(1,2);
		anotherGamer.createCells(2,1);
		anotherGamer.createCells(2,2);
		assertEquals(true,gamer.equals(anotherGamer));
	}
	@Test
	public void toCheckToadPattern(){
		GameOfLife gamer=new GameOfLife();
		gamer.createCells(1,1);
		gamer.createCells(1,2);
		gamer.createCells(1,3);
		gamer.createCells(2,2);
		gamer.createCells(2,3);
		gamer.createCells(2,4);
		gamer.campare(1,1);
		gamer.campare(1,2);
		gamer.campare(1,3);
		gamer.campare(2,2);
		gamer.campare(2,3);
		gamer.campare(2,4);
			GameOfLife anotherGamer=new GameOfLife();
		anotherGamer.createCells(0,2);
		anotherGamer.createCells(1,1);
		anotherGamer.createCells(1,4);
		anotherGamer.createCells(2,1);
		anotherGamer.createCells(2,4);
		anotherGamer.createCells(3,3);

		assertEquals(true,gamer.equals(anotherGamer));
	}

			@Test
	public void toCheckBoatpattern(){
		GameOfLife gamer=new GameOfLife();
		gamer.createCells(0,1);
		gamer.createCells(1,0);
		gamer.createCells(2,1);
		gamer.createCells(0,2);
		gamer.createCells(1,2);
		gamer.campare(0,1);
		gamer.campare(1,0);
		gamer.campare(2,1);
		gamer.campare(0,2);
		gamer.campare(1,2);
			GameOfLife anotherGamer=new GameOfLife();
		anotherGamer.createCells(0,1);
		anotherGamer.createCells(1,0);
		anotherGamer.createCells(0,2);
		anotherGamer.createCells(2,1);
		anotherGamer.createCells(1,2);

		assertEquals(true,gamer.equals(anotherGamer));
	}
	@Test
	public void checkOForBlinkerPattern(){
		GameOfLife gamer=new GameOfLife();
		gamer.createCells(1,1);
		gamer.createCells(1,0);
		gamer.createCells(1,2);
		gamer.campare(1,1);
		gamer.campare(1,2);
		gamer.campare(1,0);
		GameOfLife anotherGamer=new GameOfLife();
		anotherGamer.createCells(1,1);
		anotherGamer.createCells(0,1);
		anotherGamer.createCells(2,1);
		assertEquals(true,gamer.equals(anotherGamer));
	}

	@Test
	public void checkOForNegativeCoordinates(){
		GameOfLife gamer=new GameOfLife();
		gamer.createCells(-1,1);
		gamer.createCells(4,0);
		gamer.createCells(-8,2);
		gamer.campare(-1,1);
		gamer.campare(4,0);
		gamer.campare(-8,2);
		GameOfLife anotherGamer=new GameOfLife();
		assertEquals(true,gamer.equals(anotherGamer));
	}


}
