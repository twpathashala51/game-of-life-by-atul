package twpathashala51.GameOfLife;

// Parses the input
public class GameHelper {

	String[] Coordinates;

	public GameHelper(String[] Coordinates) {

		this.Coordinates = Coordinates;
	}

	public void parseCoordinates() {

		GameOfLife gamer = new GameOfLife();
		int TotalNumberOfLiveCell = Coordinates.length;
		int[] sizeOfCell = new int[2];
		for (int i = 0; i < TotalNumberOfLiveCell; i++) {
			sizeOfCell = parseCoordinates(i);
			gamer.createCells(sizeOfCell[0], sizeOfCell[1]);
		}
		for (int i = 0; i < TotalNumberOfLiveCell; i++) {
			sizeOfCell = parseCoordinates(i);
			gamer.campare(sizeOfCell[0], sizeOfCell[1]);
		}
		gamer.getCurrentGridStatus();
	}

	public int[] parseCoordinates(int i) {

		int[] size = new int[2];
		String[] cellPoints;
		cellPoints = Coordinates[i].split(",");
		size[0] = Integer.parseInt(cellPoints[0]);
		size[1] = Integer.parseInt(cellPoints[1]);
		return size;
	}
}