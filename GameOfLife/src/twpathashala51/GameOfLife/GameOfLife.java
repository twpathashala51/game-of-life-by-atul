package twpathashala51.GameOfLife;

// Does one tick(One Run Of Application)


import java.util.HashSet;

public class GameOfLife {
	HashSet<Cell> liveCells = new HashSet<>();
	HashSet<Cell> liveCellsDuplicate = new HashSet<>();

	public void createCells(int xCoordinates, int yCoordinates) {
		liveCellsDuplicate.add(new Cell(xCoordinates, yCoordinates));
		liveCells.add(new Cell(xCoordinates, yCoordinates));
	}
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		GameOfLife that = (GameOfLife) o;
		return liveCellsDuplicate != null ? liveCellsDuplicate.equals(that.liveCellsDuplicate) : that.liveCellsDuplicate == null;
	}

	public void campare(int xCoordinates, int yCoordinates) {

		int trueCount = 0;
		for (int i = xCoordinates - 1; i <= xCoordinates + 1; i++) {
			for (int j = yCoordinates - 1; j <= yCoordinates + 1; j++) {
				if(i==xCoordinates&&j==yCoordinates)continue;
				if (liveCells.contains(new Cell(i, j))) trueCount++;
				else checkForDeadCells(i,j);
			}
		}
		removeDeadCells(xCoordinates, yCoordinates, trueCount);
	}

	public void removeDeadCells(int xCoordinates, int yCoordinates, int trueCount) {
		System.out.println(xCoordinates + "," + yCoordinates + "=" + trueCount);
		if (trueCount < 2) liveCellsDuplicate.remove(new Cell(xCoordinates, yCoordinates));
		if (trueCount > 3) liveCellsDuplicate.remove(new Cell(xCoordinates, yCoordinates));
		if (trueCount == 2 || trueCount == 3) liveCellsDuplicate.add(new Cell(xCoordinates, yCoordinates));
	}

	public void getCurrentGridStatus() {
		for (Cell cell : liveCellsDuplicate) {
			System.out.println(cell.getxCoordinates() + "," + cell.getyCoordinates());
		}
	}

	public void checkForDeadCells(int xCoordinates,int yCoordinates) {
		int trueCount = 0;
			for (int i = xCoordinates - 1; i <= xCoordinates + 1; i++){
				for (int j = yCoordinates - 1; j <= yCoordinates + 1; j++) {
					if (i == xCoordinates && j == yCoordinates) continue;
					if (liveCells.contains(new Cell(i, j))) trueCount++;
				}	}
				if (trueCount == 3)
					liveCellsDuplicate.add(new Cell(xCoordinates, yCoordinates));
	}
}




