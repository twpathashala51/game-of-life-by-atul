package twpathashala51.GameOfLife;

// Represents a single Cell

public class Cell {

	private int xCoordinates, yCoordinates;

	public Cell(int xCoordinates, int yCoordinates) {
		this.xCoordinates = xCoordinates;
		this.yCoordinates = yCoordinates;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Cell cell = (Cell) o;
		if (xCoordinates != cell.xCoordinates) return false;
		return yCoordinates == cell.yCoordinates;
	}

	@Override
	public int hashCode() {
		int result = xCoordinates * 3 + yCoordinates * 5;
		return result;
	}

	public int getyCoordinates() {
		return yCoordinates;
	}

	public int getxCoordinates() {
		return xCoordinates;
	}
}
