package twpathahala51.wallet;

import org.junit.Test;
import twpathashala51.wallet.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AccountTest {

	@Test
	public void shouldAddMoneyToWallet() {
		Money money = new Money(1100);
		Account account = new Account(new Money(10));
		account.add(money);
		assertEquals(new Money(1110), account.getDebitBalance());
	}

	@Test
	public void shouldAddMoneyMultipleTimes() {
		Account account = new Account(new Money(100));
		account.add(new Money(1000));
		account.add(new Money(1000));
		assertEquals(new Money(2100), account.getDebitBalance());
	}

	@Test
	public void shouldDeductMoneyFromWallet() {
		Account account = new Account(new Money(100));
		account.add(new Money(1000));
		account.withdraw(new Money(1000));
		assertEquals(new Money(100), account.getDebitBalance());
		assertEquals(new Money(0), account.getCreditBalance());
	}

	@Test
	public void shouldWithdrawMoneyFromWalletEvenIfNoSufficientBalance() {
		Account account = new Account(new Money(100));
		account.add(new Money(1000));
		account.withdraw(new Money(3000));
		assertEquals(new Money(1900), account.getCreditBalance());
		assertEquals(new Money(0), account.getDebitBalance());
		account.add(new Money(2000));
		assertEquals(new Money(100), account.getCreditBalance());

	}

	@Test
	public void dummyObserverShouldGetNotifiedWhenAccountIsOverdrawn() {
		Account account = new Account(new Money(100));
		account.add(new Money(1000));
		DummyAccountOverdrawListener listener = new DummyAccountOverdrawListener();
		account.addNotificationListener(listener);
		account.withdraw(new Money(3000));
		assertTrue(listener.wasNotified);

	}

	@Test
	public void dummyObserverShouldGetNotifiedWhenAccountIsOverdrawnMultipleTimes() {
		Account account = new Account(new Money(100));
		account.add(new Money(1000));
		DummyAccountOverdrawListener listener = new DummyAccountOverdrawListener();
		SecondDummyAccountOverdrawListener secondListener = new SecondDummyAccountOverdrawListener();
		account.addNotificationListener(listener);
		account.addNotificationListener(secondListener);
		account.withdraw(new Money(3000));
		assertTrue(listener.wasNotified);
		assertTrue(secondListener.wasNotified);


	}

	private class DummyAccountOverdrawListener implements AccountOverdrawListener {
		boolean wasNotified;
		String message = "Your Account is overdrawn";

		@Override
		public void notifyCustomer(String notificationMessage) {
			this.wasNotified = true && message == notificationMessage;
		}
	}

	private class SecondDummyAccountOverdrawListener implements AccountOverdrawListener {
		boolean wasNotified;
		String message = "Your Account is overdrawn";

		@Override
		public void notifyCustomer(String notificationMessage) {
			this.wasNotified = true && message == notificationMessage;
		}
	}
}
