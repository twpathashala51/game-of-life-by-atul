package twpathahala51.wallet;

import org.junit.Assert;
import org.junit.Test;
import twpathashala51.wallet.Money;

/**
 * Created by atulk on 7/28/16.
 */
public class MoneyTest {

	@Test
	public void addMoneyToWallet() {
		Money money = new Money(1000);
		money.add(money);
		Assert.assertEquals(money,new Money(1000));
	}
	@Test
	public void removeMoneyToWallet() {
		Money money = new Money(1000);
		money.add(money);
		money.subtract(new Money(200));
		Assert.assertEquals(money,new Money(800));
	}
}
