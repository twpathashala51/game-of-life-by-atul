package twpathashala51.wallet;

import java.util.ArrayList;

public class Account {

	private Money money;
	private int flag = 0;
	ArrayList<AccountOverdrawListener> accountOverdrawListenerMessage = new ArrayList<>();

	public Account(Money money) {
		this.money = money;
	}

	public void add(Money value) {
		if (flag == 1) {
			this.money = money.subtractCredit(value);
			if (money.isBalanceNegative() == true) flag = 0;
		} else
			this.money = money.add(value);
	}

	public Money withdraw(Money value) {
		this.money = money.subtract(value);
		if (money.isBalanceNegative() == true) {
			this.money = money.getCreditBalance();
			flag = 1;
			notifyCustomer();

		}
		return value;
	}

	public void addNotificationListener(AccountOverdrawListener accountOverdrawListenerListener) {
		accountOverdrawListenerMessage.add(accountOverdrawListenerListener);
	}

	private void notifyCustomer() {

		for (AccountOverdrawListener listener : accountOverdrawListenerMessage)
			listener.notifyCustomer("Your Account is overdrawn");
	}

	public Money getDebitBalance() {
		if (flag == 1) return new Money(0);
		else return money;
	}

	public Money getCreditBalance() {
		if (flag == 0) return new Money(0);
		else return money;
	}
}

