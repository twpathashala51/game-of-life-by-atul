package twpathashala51.wallet;

// A denomination of Money
public class Money {


	private final double amount;

	@Override
	public String toString() {
		return "add{" +
				"amount=" + amount +
				'}';
	}

	public Money(double amount) {
		this.amount = amount;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Money money = (Money) o;

		return Double.compare(money.amount, amount) == 0;

	}

	@Override
	public int hashCode() {
		return (int) (amount + 30);
	}

	public Money add(Money value) {
		return new Money(amount + value.amount);
	}

	public Money subtract(Money value) {

		return new Money(amount - value.amount);
	}

	public boolean isBalanceNegative() {
		return amount < 0;
	}

	public Money getCreditBalance() {
		return new Money(-(amount));
	}

	public Money subtractCredit(Money value) {
		if (amount > value.amount)
			return new Money(amount - value.amount);
		else return new Money(value.amount - amount);
	}

}