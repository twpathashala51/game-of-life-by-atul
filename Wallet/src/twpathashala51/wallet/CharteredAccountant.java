package twpathashala51.wallet;

import java.util.ArrayList;

// Notifies Chartered Accountant
public class CharteredAccountant implements AccountOverdrawListener {

	public void notifyCustomer(String notificationMessage) {
		ArrayList<String> notificationList = new ArrayList<>();
		notificationList.add(notificationMessage);
		for (String msg : notificationList)
			System.out.println("AccountOverdrawListener to CA " + msg);


	}
}
