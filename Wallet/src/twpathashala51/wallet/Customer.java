package twpathashala51.wallet;

import java.util.*;

// AccountOverdrawListener to the customer
 public class Customer implements AccountOverdrawListener {

	public void notifyCustomer(String notificationMessage) {
		ArrayList<String> notificationList=new ArrayList<>();
		notificationList.add(notificationMessage);
		for(String msg:notificationList)
			System.out.println("AccountOverdrawListener to Customer "+msg);

	}
}
